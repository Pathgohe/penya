@extends('layouts.app')

@section('title', 'Roles show')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
    <h1>
        Este es el detalle de roles <?php echo $role->id ?>
    </h1>

    <ul>
        <li>Nombre: {{ $role->name }}</li>


    </ul>
     <h2>Lista de usuarios</h2>
    <ul>
    @foreach ($role->users as $user)
        <li>{{ $user->name }} / {{ $user->email }}</li>
    @endforeach
    </ul>
</div>
</div>
</div>
@endsection
