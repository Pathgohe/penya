@extends('layouts.app')

@section('title', 'Producto')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
    <h1>
        Este es el detalle del producto <?php echo $product->id ?>
    </h1>

    <ul>
        <li>Nombre: {{ $product->name }}</li>
        <li>Precio: {{ $product->price }}</li>
        <li>Categoria:
            @forelse ($cathegories as $cathegory)
                @if($cathegory->id == $product->cathegory_id)
                {{ $cathegory->name }}
                @endif
            @empty
                <li>No hay categoría!!</li>
            @endforelse
        </li>
    </ul>
    <a href="/products/"> Volver </a>
</div>
</div>
</div>
@endsection

