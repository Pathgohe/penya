@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')

<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
    <h1>Alta de productos</h1>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    {{--
    --}}

    @if ($errors->any())
        <div class="alert alert-danger">
            Se han producido errores de validación
        </div>
    @endif

    <form method="post" action="/products">
        {{ csrf_field() }}

        <label>Nombre</label>
        <input type="text" name="name" value="{{ old('name') }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>

        <label>Precio</label>
        <input type="text" name="price" value="{{ old('price') }}">
        <div class="alert alert-danger">
            <!--{{ $errors->first('price') }}-->
        </div>

        <br>
        <label>Categoría</label>
        <select name="cathegory_id">
            @foreach ($cathegories as $cathegory)
            <option value="{{ $cathegory->id }}"
            {{ old('cathegory_id') == $cathegory->id ? "selected='selected'": ''}}>
            {{ $cathegory->name}}</option>

             @endforeach
        <div class="alert alert-danger">
            {{ $errors->first('cathegory_id') }}
        </div>
    </select>
        <br>

        <input type="submit" value="Nuevo">
    </form>
</div>
</div>
</div>
@endsection
