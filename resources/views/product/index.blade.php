@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">

    <h1>Lista de productos</h1>
    <a href="/products/create">Nuevo</a>
    <br>
    @if (!empty(@products) )
    <table class="table table-striped table-hover">

        <tr>
            <th> Nombre del producto</th>
            <th> Precio</th>
            <th>Categoría</th>
            <th colspan="2"> Acciones </th>
        </tr>
        @forelse ($products as $product)
        <tr>
            <td>{{ $product->name }}</td>
            <td> {{ $product->price }}</td>
            <td> {{ $product->cathegory->name }}</td>
            <td><a href="/products/{{ $product->id }}/edit">Editar </a>
            <a href="/products/{{ $product->id }}"> - Ver</a>
            </td>
            <td><form method="post" action="/products/{{ $product->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar" style="float: left;">
            </form>
            <a class="btn btn-success" href="/basket/{{ $product->id }}">A la cesta</a>
        </td>
        </tr>

    @empty
        <li>No hay productos!!</li>
    @endforelse
    </ul>
    @else
    <p>No existen productos en la lista</p>
    @endif
    {{ $products->render() }}
</table>
</div>
</div>
</div>
@endsection
