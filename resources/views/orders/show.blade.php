@extends('layouts.app')

@section('title', 'OrderShow')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
    <h1>
        Detalle del pedido <?php echo $order->id ?>
    </h1>
        <h2>Pedido de:{{ $user->name }}</h2>
        <h2>Pagado:
            @if($order->paid == 0)
            No
            <form method="post" action="/orders/{{ $order->id }}/paid">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="post">
                <input type="submit" value="Pagar">
            </form>
            @else
            Si
            @endif </h2>
        <table class="table table-striped table-hover">
        <tr>
            <th>Producto</th>
            <th>Cantidad</th>
            <th>Precio</th>

        </tr>
        <tr>
            @forelse ($order->products as $product)
                <td>{{ $product->name }}</td>
                <td>{{ $product->pivot->quantity }}</td>
                <td>{{ $product->price}}</td>
        </tr>
        @empty
                <p>No hay producto!!</p>
            @endforelse
        </table>
        <h2>Precio total del pedido: {{ $order->total($order->id)}}</h2>
    <a href="/orders/"> Volver </a>
</div>
</div>
</div>
@endsection

