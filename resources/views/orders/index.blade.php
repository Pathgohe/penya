@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
<h1 style="padding: 25px;">Lista de pedidos</h1>
    <a href="/products" style="padding: 25px;">Nuevo pedido</a>
    <table class="table table-striped table-hover">
        <tr>
            <th> Id de Orden</th>
            <th> Comprador</th>
            <th> Fecha</th>
            <th> Pagado </th>
            <th> Acciones</th>
        </tr>
    @forelse ($orders as $order)
        <tr>
            <td>{{ $order->id }} </td>
            <td>
                @foreach ($users as $user)
                    @if($user->id == $order->user_id)
                    {{ $user->name }}
                    @endif
                @endforeach
                </td>
            <td> {{ $order->date }} </td>
            <td> @if($order->paid == 1)
                Si
                @else
                   No
                   @endif
            </td>
            <td>
                <a href="/orders/{{ $order->id }}">Ver  </a>
                <form method="post" action="/orders/{{ $order->id }}/paid">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
            </form>




        </td>
        </tr>
    @empty
        <p>No hay pedidos!!</p>
    @endforelse

    {{ $orders->render() }}
</table>
</div>
</div>
</div>
@endsection
