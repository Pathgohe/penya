@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Cesta de la compra</h1>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre Producto</th>
            <th colspan="2">Cantidad</th>
            <th>Precio</th>
            <th>Importe</th>
            <th>Operaciones</th>
          </tr>
        </thead>

        <tbody>


          @forelse ($products as $product)
          <tr>
            <td>{{ $product->name }}</td>
            <td>{{ $product->cantidad }}</td>
            <td><a class="btn btn-primary" href="/basket/{{ $product->id }}/up"> + </a>
              <a class="btn btn-primary" href="/basket/{{ $product->id }}/down"> - </a>
            </td>
            <td>{{ $product->price }}</td>
            <td>{{ $product->total }}</td>
            <td>
               <form method="post" action="/basket/{{ $product->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
            </form>
            </td>

          </tr>

          @empty
          <tr><td colspan="6">No hay productos!!</td></tr>
          @endforelse
        </tbody>
        <tr>
          <td colspan="3"></td>
            <td>Total -------</td>
            <td colspan="2">{{$totalCal}}</td>
          </tr>
      </table>

      <a class="btn btn-primary" href="/basket/flush">Vacíar lista</a>
      <form method="post" action="/basket">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="post">
                <input type="submit" value="Guardar Pedido">
            </form>

    </div>
  </div>
</div>
@endsection
