@extends('layouts.app')

@section('title', 'Usuarios')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
    <h1>
        Este es el detalle del usuario <?php echo $user->id ?>
    </h1>

    <ul>
        <li>Nombre: {{ $user->name }}</li>
        <li>Email: {{ $user->email }}</li>
        <li>Creación: {{$user ->created_at}}</li>


    </ul>
</div>
</div>
</div>
@endsection

