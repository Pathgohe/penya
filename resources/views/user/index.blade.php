@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
    <h1>Lista de usuarios</h1>
    <a href="/users/create" style="padding: 15px;">Nuevo</a>
    <table class="table table-striped table-hover">

        <tr>
            <th> Id</th>
            <th> Nombre de usuario</th>
            <th> Mail</th>
            <th> Acciones</th>

        </tr>
        @forelse ($users as $user)

        <tr>
            <td>{{ $user->id }}</td>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>
            @can('update', $user)
            <a href="/users/{{ $user->id }}/edit">Editar</a>
            @endcan

            <a href="/users/{{ $user->id }}">Ver</a>

            @can('delete', $user)
            <form method="post" action="/users/{{ $user->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">


                <input type="submit" value="borrar">

            </form>
            @endcan

        </td>
    </tr>
    @empty
        <li>No hay usuarios!!</li>
    @endforelse
    </table>

    {{ $users->render() }}
</div>
</div>
</div>
@endsection
