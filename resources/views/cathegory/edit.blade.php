@extends('layouts.app')

@section('title', 'Categorias')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
    <h1>Editar categoría</h1>

    <form method="post" action="/cathegories/{{ $cathegory->id }}">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="PUT">

        <label>Nombre</label>
        <input type="text" name="name"
        value="{{ old('name') ? old('name') : $cathegory->name }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>

        <a href="/cathegories"> Volver</a>

        <br>

        <input type="submit" value="Guardar Cambios">
    </form>
</div>
</div>
</div>
@endsection
