@extends('layouts.app')

@section('title', 'ShowCathegory')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">

    <h1>
        Este es el detalle de la categoría:  <?php echo $cathegory->name ?>
    </h1>

    <ul>
        <li>Id: {{ $cathegory->id }}</li>
        <li>Nombre: {{ $cathegory->name }}</li>

    </ul>
    <a href="/cathegories/{{ $cathegory->id }}/edit">Editar - </a>
            <a href="/cathegories"> Volver</a>
    <h2>Lista de productos de esta categoría</h2>
    <table class="table table-striped table-hover">
        <tr>
            <th>Nombre</th>
            <th>Precio</th>
        </tr>

    @foreach($cathegory->productos as $product)
        <tr>
            <td>{{$product->name}}</td>
            <td>{{$product->price}}</td>
        </tr>
    @endforeach
    </table>
</div>
</div>
</div>



@endsection
