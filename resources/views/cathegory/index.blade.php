@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
<h1 style="padding: 25px;">Lista de categorías</h1>
    <a href="/cathegories/create" style="padding: 25px;">Nueva</a>

    <table class="table table-striped table-hover">
        <tr>
            <th> Id de Categoría</th>
            <th> Nombre de Categoría</th>
            <th colspan="2"> Acciones </th>
        </tr>
    @forelse ($cathegories as $cathegory)
        <tr>
            <td>{{ $cathegory->id }} </td>
            <td> {{ $cathegory->name }} </td>
            <td> <a href="/cathegories/{{ $cathegory->id }}/edit">Editar </a> -
            <a href="/cathegories/{{ $cathegory->id }}">Ver  </a>
            </td>
            <td><form method="post" action="/cathegories/{{ $cathegory->id }}">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="submit" value="borrar">
            </form>




        </td>
        </tr>
    @empty
        <p>No hay categorías!!</p>
    @endforelse

    {{ $cathegories->render() }}
</table>
</div>
</div>
</div>
@endsection
