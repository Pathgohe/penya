<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    //como estamos en order, buscara una tabla que se llame order_products
    //si la tabla se llamase de otra manera
    // return $this->belongsToMany('App\Role(clase)', 'nombre_tabla');
    protected $fillable = [
        'product_id', 'quantity', 'price'
    ];

    public function products(){
        //con esto lee las filas que hay en la base de datos
        //si solo pudiese estar el producto en un pedido seria hasmany
        return $this->belongsToMany(Product::class)->withPivot('order_id', 'product_id', 'quantity', 'price');

    }


    public static function total($id)
    {
        //return $order->product;
        $order = Order::findOrFail($id);
        $total=0;
        foreach($order->products as $product){

            $total += $product->pivot->price * $product->pivot->quantity;
        }
        return $total;
    }

}
