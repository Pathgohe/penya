<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cathegory extends Model
{


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];
    public function productos(){
        return $this->hasMany(Product::class);
    }


}
