<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cathegory;

class CathegoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }
    public function index()
    {
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        $cathegories = Cathegory::paginate(10);
        return view('cathegory.index', ['cathegories' => $cathegories]);

    }


    public function create()
    {
        return view('cathegory.create');
    }


    public function store(Request $request)
    {
        //validacion:
        $rules = [
            'name' => 'required|max:255|min:3',


        ];

        $request->validate($rules);

        $cathegory = new Cathegory();
        $cathegory->fill($request->all());
        $cathegory->save();

        return redirect('/cathegories');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cathegory = Cathegory::findOrFail($id);
        return view('cathegory.show', ['cathegory' => $cathegory]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cathegory = Cathegory::findOrFail($id);
        return view('cathegory.edit', ['cathegory' => $cathegory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /*public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|max:255|min:4',
            'price' => 'required|max:25|min:1',,
        ];

        $request->validate($rules);




        $product = Product::findOrFail($id);
        $product->fill($request->all());
        $product->save();


        return redirect('/products/' . $product->id);
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $user = User::findOrFail($id);
        // $user->delete();

        Cathegory::destroy($id);

        return back();
    }

    public function especial()
    {
        $cathegories = Cathegory::where('id', '>=', 15)
            ->where('id', '<=', 20)
            ->get();

        dd($cathegories);
        return "especial";
        return redirect('/cathegories');
        // return "Especial";
    }
}
