<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('index', User::class); //como no hay ningun objeto pasamos la clase completa
        // $users = User::all();
        $users = User::paginate(10);
        // return $users;
        return view('user.index', ['users' => $users]);
        //busca el fichero (uno de los dos):
        // /resources/views/user/index.php
        // /resources/views/user/index.blade.php

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('user.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validacion:
        $rules = [
            'name' => 'required|max:255|min:3',
            'email' => 'required|unique:users|max:255|email',
            'password' => 'required|max:255',
            // 'color' => 'required',
            // 'player' => 'required|max:255|min:5',
        ];

        $request->validate($rules);


        // $user = new User();
        // $user->name = $request->input('name');
        // $user->email = $request->input('email');
        // $user->password = bcrypt($request->input('password'));

        // $user->remember_token = str_random(10);
        // $user->save();

        // //opcion2
        // $user = User::create($request->all());

        //opcion3
        $user = new User();
        $user->fill($request->all());
        $user->password = bcrypt($user->password);
        $user->save();

        return redirect('/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // $user = User::find($id);
        // if ($user == null) {
        //     abort(404, 'Prohibido!!!!');
        //     // response()->view('errors.404', [], 404);
        // }
        $user = User::findOrFail($id);
        return view('user.show', [
            'user' => $user,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit2($id){ //esta es una manera de hacerlo y la otra es como en edit
        //este seria un metodo fachada por eso user va en minuscula
        $currentuser=\Auth::user(); //usuario logeado
        //usuario que quiero editar
        $user = User::findOrFail($id);
        if($currentuser->can('update', $user)){
            return "enhorabuena, adelante";

        }else{
            return back();
            return redirect("/users/"->$id);
        }
        dd($user);
        return "EDIT2";

    }

    public function edit($id)
    {

        $user = User::findOrFail($id);

        $this->authorize('update', $user);
        //al crear tenemos que decirle la clase de la que vamos a crear
        //$this->authorize('create', User::class);

        return view('user.edit', ['user' => $user]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|max:255|min:4',
            'email' => "required|unique:users,email,$id,id|max:255|email",
        ];

        $request->validate($rules);

        $user = User::findOrFail($id);

        $this->authorize('update', $user);
        $user->fill($request->all());
        $user->save();


        return redirect('/users/' . $user->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {


        $user = User::findOrFail($id);
        $this->authorize('delete', $user);
        $user->delete();
        //User::destroy($id);

        return back();


    }

    public function especial()
    {
        $users = User::where('id', '>=', 15)
            ->where('id', '<=', 20)
            ->get();

        dd($users);
        return "especial";
        return redirect('/users');
        // return "Especial";
    }
}
