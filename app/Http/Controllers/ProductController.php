<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cathegory;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        $products = Product::paginate(10);
        return view('product.index', ['products' => $products]);

    }


    public function create()
    {
        $cathegories= Cathegory::all();
        return view('product.create',['cathegories'=> $cathegories]);
    }


    public function store(Request $request)
    {
        //validacion:
        $rules = [
            'name' => 'required|max:255|min:3',
            'price' => 'required|max:25|min:1',

        ];

        $request->validate($rules);

        $product = new Product();
        $product->fill($request->all());
        $product->save();

        return redirect('/products');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        $cathegories= Cathegory::all();
        return view('product.show', [
            'product' => $product, 'cathegories'=> $cathegories
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $cathegories= Cathegory::all();
        return view('product.edit', ['product' => $product, 'cathegories'=> $cathegories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $rules = [
            'name' => 'required|max:255|min:4',
            //'price' => 'required|max:25|min:1',,
        ];

        $request->validate($rules);

        $product = Product::findOrFail($id);
        $product->fill($request->all());
        $product->save();


        return redirect('/products/' . $product->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $user = User::findOrFail($id);
        // $user->delete();

        Product::destroy($id);

        return back();
    }

    public function especial()
    {
        $products = Product::where('id', '>=', 15)
            ->where('id', '<=', 20)
            ->get();

        dd($products);
        return "especial";
        return redirect('/products');
        // return "Especial";
    }
}
