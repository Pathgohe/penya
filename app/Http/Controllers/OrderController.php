<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\User;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $orders = Order::paginate(10);
        $user = Auth::user();

        if($user->role_id == 3 || $user->role_id == 4){
            if($request->all == "1"){
                return $this->listaTodos();
            }else{
                return $this->listaPropios($user);
            }

        }else{
            return "No puede ver los pedidos";
        }

       // return view('orders.index', ['orders' => $orders, 'users' => $users]);

    }
    public function show($id){

        $order = Order::findOrFail($id);
        //$products=$order->products();

        $usuario= $order->user_id;
        $user = User::findOrFail($usuario);

        return view('orders.show', [
            'order' => $order, 'user'=> $user
        ]);
    }

    public function paid($id)
    {
        $order = Order::findOrFail($id);
        $order->paid = 1;
        $order->save();

        return redirect('/orders');
    }

    public function listaPropios(){

        $orders = Order::paginate(10);
        $users=User::all();
        return view('orders.index', ['orders' => $orders, 'users' => $users]);
        //return "lista propios";
    }
    public function listaTodos($user){

        $orders = Order::paginate(10);
        $user = User::findOrFail($user);
        return view('orders.index', ['orders' => $orders, 'user' => $user]);
        //return "lista todos";
    }
}
