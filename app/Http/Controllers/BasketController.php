<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Order;
use App\User;
use Carbon\Carbon; //para la fecha

use Session; //es un modelo que ya existe


class BasketController extends Controller
{
    public function index(Request $request)
    {
        $basket = $request->session()->get('basket');
        if (! $basket) {$basket = [];}
        $totalCal=0;
        //con este foreach guardamos el precio de cada producto por su cantidad, osea, el importe de cada fila
        foreach ($basket as $key => $item) {
                $item->total = $item->price*$item->cantidad;
                $totalCal=$item->total+$totalCal;
        }
        return view('basket.index', ['products' => $basket], ['totalCal' =>$totalCal]);

    }
    //flush vacia la cesta
     public function flush(Request $request)
    {
        $request->session()->forget('basket');
        return back();
    }
    public function addProduct(Request $request, $id)
    {
        //buscar usuario
        $product = Product::findOrFail($id);

        $basket = $request->session()->get('basket');

        if ($basket == null) {
            $basket = array();
        }

        $position = -1;
        foreach ($basket as $key => $item) {
            if ($item->id == $id) {
                $item->cantidad++;
                $position = $key;
                break;
            }
        }
        if ($position == -1) {
            $product->cantidad = 1;
            $request->session()->push('basket', $product);
        }
        return redirect('/basket');
    }//final de añadir producto a la cesta

    public function up(Request $request, $id){
        $product = Product::findOrFail($id);

        $basket = $request->session()->get('basket');
        foreach ($basket as $key => $item) {
            if ($item->id == $id) {
                $item->cantidad++;
                $position = $key;
                break;
            }

        }
        return redirect('/basket');
    }//final de up ---- añadir cantidad de un producto

    public function down(Request $request, $id){
        $product = Product::findOrFail($id);
        $basket = $request->session()->get('basket');
        foreach ($basket as $key => $pro) {
            if ($product->id == $basket[$key]->id) {
                if($pro->cantidad<=1){
                    $request->session()->forget('basket.' .$key);
                }else{
                    $pro->cantidad--;
                }
                 return back();
            }

        }
        return redirect('/basket');
    }//final del down--- para quitar cantidad de productos

    public function delete(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $basket = $request->session()->get('basket');
        //esto borra todos los pedidos de la cesta

        foreach ($basket as $key => $prod) {
            //aqui comparamos los productos de la cesta con el producto que llega $id
            if($product->id == $basket[$key]->id){
                if($prod->cantidad<=1){
                    //return $key;
                    $request->session()->forget('basket.' .$key);
                }else{
                    $prod->cantidad--;
                }
                return back();
            }

        }

        return back();

    }

    public function store(Request $request)
    {
        $basket = $request->session()->get('basket');
        //use Order
        $order = New Order;
        //usuario se saca de la sesion
        $user=auth()->user();
        $order->user_id = $user->id;
        //para la fecha usamos Carbon
        $carbon = new Carbon;
        $date = $carbon->now();
        //para sacar formato: $carbon->now()->format('d-m-Y');
        $order->date = $date;
        $order->save();

        foreach($basket as $product){
            $order->products()->attach($product->id,
                ['quantity'=>$product->cantidad, 'price'=>$product->price]
                );
        }
        return redirect('/basket/flush');
    }//final de store

    public static function contador()
    {
        $basket = session()->get('basket');
        //return $basket;
        $totalCantidad=0;
        if($basket == null){
            return $totalCantidad;
        }else{
            foreach($basket as $product){
            $totalCantidad += $product->cantidad;
        }
        }

        return $totalCantidad;

    }


}//final de la clase basketcontroller

