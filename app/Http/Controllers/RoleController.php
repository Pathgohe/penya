<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{

    public function index()
    {
        /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
        $this->authorize('index', Role::class);
        $roles = Role::paginate(10);
        return view('role.index', ['roles' => $roles]);

    }



    /*public function especial()
    {
        $products = Product::where('id', '>=', 15)
            ->where('id', '<=', 20)
            ->get();

        dd($products);
        return "especial";
        return redirect('/products');
        // return "Especial";
    }*/

    public function show($id){
        $role=Role::with('users')->findOrFail($id);
        $this->authorize('view', $role);
        return view('role.show',['role'=>$role]);
    }
}
