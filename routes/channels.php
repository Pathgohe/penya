<?php

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});
Broadcast::channel('App.Product.{id}', function ($product, $id) {
    return (int) $product->id === (int) $id;
});
Broadcast::channel('App.Cathegory.{id}', function ($cathegory, $id) {
    return (int) $cathegory->id === (int) $id;
});
Broadcast::channel('App.Role.{id}', function ($role, $id) {
    return (int) $role->id === (int) $id;
});
