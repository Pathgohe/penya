<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('users', 'UserController@index')->name('usuarios');
// Route::get('users/{id}', 'UserController@show');
// Route::get('users/{id}/edit', 'UserController@edit');
// Route::get('users/create', 'UserController@create');
// Route::put('users/{id}', 'UserController@update');
// Route::delete('users/{id}', 'UserController@destroy');
// Route::post('users', 'UserController@store');

//Ruta de tipo resource: equivale a las 7 rutas REST
//Ruta especial antes que resource, si no "show...."
Route::get('users/especial', 'UserController@especial');
Route::get('users/{id}/edit2', 'UserController@edit2');
Route::resource('users', 'UserController');

Route::get('products/especial', 'ProductController@especial');
Route::resource('products', 'ProductController');



Route::get('cathegories/especial', 'CathegoryController@especial');
Route::resource('cathegories', 'CathegoryController');

Route::get('roles/especial', 'RoleController@especial');
Route::resource('roles', 'RoleController');
////////////////////////////////////////////
//Ejemplos de rutas con funciones anónimas:
////////////////////////////////////////////

Route::get('usuarios/{id}', function ($id) {
   return "Detalle del usuario $id";
});


Route::get('usuarios/{id}', function ($id) {
   return "Detalle del usuario $id";
})->where('id', '[0-9]+');

Route::get('usuarios/{id}/{name?}', function ($id, $name=null) {
    if($name) {
        return "Detalle del usuario $id. El nombre es $name";
    } else {
        return "Detalle del usuario $id. Anónimo";
    }
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/basket','BasketController@index');
Route::get('/basket/flush','BasketController@flush');
Route::get('/basket/{id}', 'BasketController@addProduct');
Route::delete('/basket/{id}', 'BasketController@delete');
Route::get('/basket/{id}/up','BasketController@up');
Route::get('/basket/{id}/down', 'BasketController@down');
Route::post('/basket','BasketController@store');

Route::get('/orders','OrderController@index');
Route::get('/orders/{id}','OrderController@show');
Route::post('/orders/{id}/paid','OrderController@paid');

//ejemplo para la utilización de fecha
Route::get('/hoy', function(){
    $today = new \Carbon\Carbon();
    $date = $today->today();

    return $date->format('d-m-Y');
});


